package api;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import domain.house.BuildingType;
import domain.house.DealType;
import domain.house.House;
import domain.user.*;

@WebServlet("/api/AddNewHouse")
public class AddNewHouseAPI extends HttpServlet {
	private static int idCount = 0;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(200);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		
		try {
			if (!(req.getAttribute(("user")) instanceof Individual)) {
				resp.setStatus(401);
				out.print("{\"loggedIn\":false}");
				return;
			}
			
			House newHouse = new House(
					((User) req.getAttribute("user")).getUserId(),
					Integer.parseInt("0" + req.getParameter("area")),
					req.getParameter("building_type").equals(BuildingType.villa.name()) ? BuildingType.villa :
							req.getParameter("building_type").equals(BuildingType.apartment.name()) ? BuildingType.apartment : null,
					req.getParameter("address"),
					req.getParameter("deal_type").equals(DealType.sell.name()) ? DealType.sell :
							req.getParameter("deal_type").equals(DealType.rent.name()) ? DealType.rent : null,
					Integer.parseInt("0" + req.getParameter("base_price")),
					Integer.parseInt("0" + req.getParameter("rent_price")),
					Integer.parseInt("0" + req.getParameter("sell_price")),
					req.getParameter("phone"),
					req.getParameter("description"));
			newHouse.setId(Integer.toString(idCount++));
			((Individual) req.getAttribute("user")).addHouse(newHouse);
			out.print("{\"success\": \"Ok\",\"loggedIn\":true}");
		} catch (Exception e) {
			out.print("{\"success\":\"NotOk\",\"message\":\"" + e.getMessage() + "\"}");
			System.out.println(e.getMessage());
		}
	}
}
