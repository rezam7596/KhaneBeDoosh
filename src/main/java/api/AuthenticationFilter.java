package api;

import domain.authentication_server.AuthenticationServer;
import domain.user.User;
import domain.user.UserDB;
import io.jsonwebtoken.Claims;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
//        this.context = filterConfig.getServletContext();
//        this.context.log("AuthenticationFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        try {
            String jwt = ((HttpServletRequest) servletRequest).getHeader("JWT");
            Claims claims = AuthenticationServer.getInstance().parseJWT(jwt);
            if (claims != null){
                User loggedInUser = UserDB.getInstance().getUserByUserId(claims.getId());
                servletRequest.setAttribute("user", loggedInUser);
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
//      close any resources here
    }
}
