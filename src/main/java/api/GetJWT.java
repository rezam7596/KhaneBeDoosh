package api;

import domain.authentication_server.AuthenticationServer;
import domain.user.User;
import domain.user.UserDB;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/api/GetJWT")
public class GetJWT extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(200);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        try {
            String username = req.getParameter("username");
            String password = req.getParameter("password");
            User user = UserDB.getInstance().getUser(username, password);
            if(user == null){
                resp.setStatus(401);
                out.print("{\"success\":false}");
                return;
            }
            String JWT = AuthenticationServer.getInstance().createJWT(user.getUserId(), "TheAuthenticationServer", 30*60*1000);

            out.print("{");
            out.print("\"success\":true,");
            out.print("\"jwt\":\"" + JWT + "\"");
            out.print("}");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
