package api;

import javax.servlet.annotation.WebServlet;

import domain.house.House;
import domain.house.HouseDB;
import domain.user.Individual;
import domain.user.UserDB;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

@WebServlet("/api/GetPhoneNumber")
public class GetPhoneNumberAPI extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(200);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		
		try {
			if (!(req.getAttribute(("user")) instanceof Individual)) {
				resp.setStatus(401);
				out.print("{\"loggedIn\":false}");
				return;
			}

			Individual user = (Individual) req.getAttribute("user");
			House house = HouseDB.getInstance().findHouseById(req.getParameter("houseId"));
			if (user.isHouseNumPaid(house.getId())) {
				out.print("{\"success\":true,\"message\":\"already paid\",\"loggedIn\":true}");
			} else if (user.getBalance() >= 1000) {
				user.setBalance();
				user.paidHouse(house.getId());
				out.print("{\"success\":true,\"message\":\"paid successfully\",\"loggedIn\":true}");
			} else {
				out.print("{\"success\":false,\"message\":\"not enough money\",\"loggedIn\":true}");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}