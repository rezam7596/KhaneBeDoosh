package api;

import domain.user.Individual;
import domain.user.User;
import domain.user.UserDB;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/api/GetUserInfo")
public class GetUserInfoAPI extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(200);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();

		try {
			if (!(req.getAttribute(("user")) instanceof Individual)) {
				resp.setStatus(401);
				out.print("{\"loggedIn\":false}");
				return;
			}

			String userInfo = ((User) req.getAttribute("user")).getUserInfo();
			out.print(userInfo.substring(0, userInfo.length() - 1) + ",\"loggedIn\":true}");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
