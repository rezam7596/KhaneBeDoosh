package api;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import domain.house.House;
import domain.house.HouseDB;
import domain.user.*;

@WebServlet("/api/HouseInfo")
public class HouseInfoAPI extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(200);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		try {
			House house = HouseDB.getInstance().findHouseById(req.getParameter("houseId"));
			if(house == null){
				resp.setStatus(404);
				out.print("{\"message\":\"Invalid House ID\"}");
				return;
			}

			String houseInfo = new ObjectMapper().writeValueAsString(house);

			if(!(req.getAttribute(("user")) instanceof Individual)){
				houseInfo = houseInfo.substring(0, houseInfo.length() - 2) + ",\"paid\":false,\"loggedIn\":false}";
				houseInfo = houseInfo.replaceFirst("\"phone\":[^,}]+", "\"phone\":\"***\"");
				out.print(houseInfo);
				return;
			}

			Individual user = (Individual) req.getAttribute("user");
			if (user.isHouseNumPaid(house.getId())) {
				houseInfo = houseInfo.substring(0, houseInfo.length() - 1) + ",\"paid\":true,\"loggedIn\":true}";
			} else {
				houseInfo = houseInfo.substring(0, houseInfo.length() - 2) + ",\"paid\":false,\"loggedIn\":true}";
				houseInfo = houseInfo.replaceFirst("\"phone\":[^,}]+", "\"phone\":\"***\"");
			}

			out.print(houseInfo);
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
}
