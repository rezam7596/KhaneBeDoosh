package api;

import domain.Message;
import domain.user.Individual;
import domain.user.UserDB;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/api/IncreaseCredit")
public class IncreaseCreditAPI extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(200);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();

		try {
			if(!(req.getAttribute(("user")) instanceof Individual)){
				resp.setStatus(401);
				out.print("{\"loggedIn\":false}");
				return;
			}

			int credit = Integer.parseInt(req.getParameter("balance"));
			Message responseMessage = ((Individual) req.getAttribute("user")).pay(credit);
			if(responseMessage.getSuccess())
				req.setAttribute("messageToShowInTop", "Your Credit Increased successfully");
			else
				req.setAttribute("messageToShowInTop", responseMessage.getMessage());

			out.print("{");
			out.print("\"success\":" + (responseMessage.getSuccess()?"true":"false") + ",");
			out.print("\"message\":\"" + req.getAttribute("messageToShowInTop") +"\",");
			out.print("\"loggedIn\":true");
			out.print("}");
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
}
