package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import domain.house.*;
import domain.user.UserDB;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/api/SearchHouses")
public class SearchHousesAPI extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(200);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        try {
            UserDB.getInstance(); //to create users
            int minimumArea;
            try {
                minimumArea = Integer.parseInt(req.getParameter("minimum_area"));
            } catch (Exception e) {
                minimumArea = 0;
            }
            BuildingType buildingType = null;
            if (req.getParameter("building_type").equals(BuildingType.villa.name()))
                buildingType = BuildingType.villa;
            else if (req.getParameter("building_type").equals(BuildingType.apartment.name()))
                buildingType = BuildingType.apartment;
            DealType dealType = null;
            if (req.getParameter("deal_type").equals(DealType.sell.name()))
                dealType = DealType.sell;
            else if (req.getParameter("deal_type").equals(DealType.rent.name()))
                dealType = DealType.rent;
            int maximumPrice;
            try {
                maximumPrice = Integer.parseInt(req.getParameter("maximum_price"));
            } catch (Exception e) {
                maximumPrice = Integer.MAX_VALUE;
            }

            List<House> houses = HouseDB.getInstance().searchAllHouses(minimumArea, buildingType, dealType, maximumPrice);
            for (House house:houses) {
                house.replaceSpecialChars();
            }

            out.print(new ObjectMapper().writeValueAsString(houses));
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
