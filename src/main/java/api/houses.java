package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import domain.house.House;
import domain.house.HouseDB;
import domain.user.Individual;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/houses")
public class houses extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(200);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        try {
            if(!(req.getAttribute(("user")) instanceof Individual)){
                resp.setStatus(401);
                out.print("{\"loggedIn\":false}");
                return;
            }

            List<House> houses;
            if(((Individual)req.getAttribute("user")).isAdmin())
                houses = HouseDB.getInstance().getAllHouses();
            else
                houses = HouseDB.getInstance().getAllHousesFromUser((Individual)req.getAttribute("user"));

            for (House house:houses) {
                house.replaceSpecialChars();
            }

            out.print(new ObjectMapper().writeValueAsString(houses));
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
