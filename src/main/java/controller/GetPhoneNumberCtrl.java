package controller;

import javax.servlet.annotation.WebServlet;

import domain.house.House;
import domain.house.HouseDB;
import domain.user.Individual;
import domain.user.UserDB;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

@WebServlet("/getPhone")
public class GetPhoneNumberCtrl extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("user", UserDB.getInstance().getUser("Homayoon"));
        Individual user = (Individual) UserDB.getInstance().getUser("Homayoon");
        String houseId = req.getParameter("houseId");
        House house = HouseDB.getInstance().findHouseById(houseId);
        req.setAttribute("targetHouse", house);
        if (!user.isHouseNumPaid(houseId)){
            if(user.getBalance() >= 1000){
                user.setBalance();
                user.paidHouse(houseId);
                req.setAttribute("owner_phone", "show");
            }
        }else {
                req.setAttribute("owner_phone", "show");
        }
        req.getRequestDispatcher("pages/houseInfo.jsp").forward(req, resp);
    }
}
