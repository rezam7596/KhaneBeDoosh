package controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import domain.house.BuildingType;
import domain.house.DealType;
import domain.house.House;
import domain.house.HouseDB;
import domain.user.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.message.*;
import org.apache.http.client.entity.*;
import org.apache.http.util.*;
import org.apache.http.*;
import org.apache.http.impl.client.*;
import com.fasterxml.jackson.core.JsonParser;
@WebServlet("/houseInfo")
public class HouseInfoPageCtrl extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("user", UserDB.getInstance().getUser("Homayoon"));
        String houseId = req.getParameter("houseId");
        House house = HouseDB.getInstance().findHouseById(houseId);
        req.setAttribute("targetHouse", house);
        req.setAttribute("owner_phone", "get_request");
        req.getRequestDispatcher("pages/houseInfo.jsp").forward(req, resp);
    }
}
