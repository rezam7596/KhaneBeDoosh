package controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import domain.Message;
import domain.house.BuildingType;
import domain.house.DealType;
import domain.house.House;
import domain.user.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.message.*;
import org.apache.http.client.entity.*;
import org.apache.http.util.*;
import org.apache.http.*;
import org.apache.http.impl.client.*;
import com.fasterxml.jackson.core.JsonParser;


@WebServlet("/increaseCredit")
public class IncreaseCreditCtrl extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("user", UserDB.getInstance().getUser("Homayoon"));
        try {
	        int credit = Integer.parseInt(req.getParameter("balance"));
	        Message responseMessage = ((Individual) req.getAttribute("user")).pay(credit);
	        if(responseMessage.getSuccess())
	        	req.setAttribute("messageToShowInTop", "Your Credit Increased successfully");
	        else
	        	req.setAttribute("messageToShowInTop", responseMessage.getMessage());
        }catch (Exception e){
	        req.setAttribute("messageToShowInTop", "Incorrect Input!");
        }
        
        req.getRequestDispatcher("main").forward(req, resp);
    }
}
