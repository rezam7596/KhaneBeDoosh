package controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import domain.user.*;

@WebServlet("/main")
public class MainPageCtrl extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("user", UserDB.getInstance().getUser("Homayoon"));
        req.getRequestDispatcher("pages/mainPage.jsp").forward(req, resp);
    }
}
