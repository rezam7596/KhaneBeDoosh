package controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import domain.house.BuildingType;
import domain.house.DealType;
import domain.house.House;
import domain.user.*;
import org.apache.http.client.methods.*;

@WebServlet("/addNewHouse")
public class addNewHouseCtrl extends HttpServlet {
	private static int idCount = 0;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("user", UserDB.getInstance().getUser("Homayoon"));
		
		House newHouse = new House(
				((User) req.getAttribute("user")).getUserId(),
				Integer.parseInt(req.getParameter("area")),
				req.getParameter("building_type").equals(BuildingType.villa.name()) ? BuildingType.villa :
				req.getParameter("building_type").equals(BuildingType.apartment.name()) ? BuildingType.apartment : null,
				req.getParameter("address"),
				req.getParameter("deal_type").equals(DealType.sell.name()) ? DealType.sell :
				req.getParameter("deal_type").equals(DealType.rent.name()) ? DealType.rent : null,
				0,
				Integer.parseInt(req.getParameter("price")),
				Integer.parseInt(req.getParameter("price")),
				req.getParameter("phone"),
				req.getParameter("description") );
		newHouse.setId(Integer.toString(idCount++));
		((Individual) req.getAttribute("user")).addHouse(newHouse);
		
		req.getRequestDispatcher("main").forward(req, resp);
	}
}
