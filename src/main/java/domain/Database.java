package domain;
import java.io.PrintStream;
import java.sql.*;

public class Database {
    private static Database db = null;
    private Connection connection = null;
    private Database(){
        try {
            System.setErr(new PrintStream("D:/error.log"));
        }catch (Exception e) {
            System.out.println("ERRoooooooooooooooR");
        }
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:D:/KhaneBeDoosh.db");
        } catch ( Exception e ) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }



    }
    public static Connection getConnection(){
        if (db == null){
            db = new Database();
        }
        return db.connection;
    }

}
