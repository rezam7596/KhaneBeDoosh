package domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
	private boolean success;
	private int code;
	private String message;
	
	public Message() {
		message = "";
	}
	
	public Message(boolean success) {
		this.success = success;
		this.message = "";
	}
	
	public Message(int code) {
		this.code = code;
		this.message = "";
	}
	public Message(String message) {
		this.message = message;
	}
	
	public Message(boolean success, int code, String message) {
		this.success = success;
		this.code = code;
		this.message = message;
	}
	
	public boolean getSuccess() {
		return success;
	}
	
	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public static String replaceSpecialChars(String input) {
		if(input == null)
			return null;
		String output = input.replace("<", "&lt");
		output = output.replace(">", "&gt");
		output = output.replace("&", "&amp");
		output = output.replace("\"", "&quot");
		output = output.replace("\'", "&#39");
		return output;
	}
}
