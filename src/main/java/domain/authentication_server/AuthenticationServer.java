package domain.authentication_server;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import io.jsonwebtoken.*;
import java.util.Date;

public class AuthenticationServer {
    private static byte[] apiKey = new String("mozi-amoo").getBytes();
    private static AuthenticationServer authServer = null;

    private AuthenticationServer(){}

    public static AuthenticationServer getInstance(){
        if (authServer == null)
            authServer = new AuthenticationServer();
        return authServer;
    }

     public String createJWT(String id, String issuer, long ttlMillis) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = apiKey;
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }
    //Sample method to validate and read the JWT
    public Claims parseJWT(String jwt) {
        try {
            //This line will throw an exception if it is not a signed JWS (as expected)
            return Jwts.parser()
                    .setSigningKey(apiKey)
                    .parseClaimsJws(jwt).getBody();
        } catch (Exception e) {
            return null;
        }
    }
}
