package domain.bank;

import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Message;
import domain.user.Individual;
import domain.user.User;
import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Payment {
	private static final String bankURL = "http://acm.ut.ac.ir/ieBank/pay";

	public static Message pay(Individual user, int credit) {
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPostRequest = new HttpPost(bankURL);
		//set header
		httpPostRequest.setHeader("apiKey", "b541b1c0-4c56-11e8-8a5f-7d010a8baae7");
		//set parameters
		HashMap<String, String> postParameters = new HashMap<>();
		postParameters.put("userId", user.getUserId());
		postParameters.put("value", String.valueOf(credit));
		try {
			String content = new ObjectMapper().writeValueAsString(postParameters);
			StringEntity entity = new StringEntity(content,	ContentType.APPLICATION_JSON);
			httpPostRequest.setEntity(entity);
			CloseableHttpResponse response = httpClient.execute(httpPostRequest);
			BufferedReader contentReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			Message message = new ObjectMapper().readValue(contentReader.readLine(), Message.class);
			message.setCode(response.getStatusLine().getStatusCode());
			response.close();
			return message;
		} catch (Exception e) {
			return new Message(false, HttpStatus.SC_NOT_ACCEPTABLE, e.getMessage());
		}
	}
}
