package domain.house;

public enum BuildingType {
	villa("villa"),
	apartment("apartment");

	private final String value;

	BuildingType(String val){
		this.value = val;
	}

	@Override
	public String toString() {
		return this.value;
	}
}
