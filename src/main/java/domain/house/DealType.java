package domain.house;

public enum DealType {
	sell("sell"),
	rent("rent");

	private final String value;

	DealType(String val){
		this.value = val;
	}
	public String getDealType() {
		return value;
	}
}
