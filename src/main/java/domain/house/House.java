package domain.house;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import domain.Message;
import domain.user.*;
import domain.user.realstate.RequestServer;
import domain.user.realstate.ServerResponseType;

@JsonDeserialize(using = HouseJsonDeserializer.class)
public class House {
	private String id;
	private String userId;
	private int area;
	private BuildingType buildingType;
	private String address;
	private String imageURL;
	private DealType dealType;
	private int basePrice;
	private int rentPrice;
	private int sellPrice;
	private String phone;
	private String description;
	private Date expireTime;
	
	public House() {
		id = String.valueOf(10E10 + ((new Random()).nextLong() % 10E9) );
		imageURL = "pics/no-pic.jpg";
	}
	
	public House(String userId, int area, BuildingType buildingType, String address, DealType dealType, int basePrice, int rentPrice, int sellPrice, String phone, String description) {
		id = String.valueOf(10E10 + ((new Random()).nextLong() % 10E9) );
		this.userId = userId;
		imageURL = "pics/no-pic.jpg";
		this.area = area;
		this.buildingType = buildingType;
		this.address = address;
		this.dealType = dealType;
		if(dealType == DealType.rent) {
			this.basePrice = basePrice;
			this.rentPrice = rentPrice;
		}else if (dealType == DealType.sell)
			this.sellPrice = sellPrice;
		this.phone = phone;
		this.description = description;
	}
	
	public House(String id, String userId, int area, BuildingType buildingType, String address, String imageURL, DealType dealType, int basePrice, int rentPrice, int sellPrice, String phone, String description, Date expireTime) {
		this.id = id;
		this.userId = userId;
		this.area = area;
		this.buildingType = buildingType;
		this.address = address;
		this.imageURL = imageURL;
		this.dealType = dealType;
		this.basePrice = basePrice;
		this.rentPrice = rentPrice;
		this.sellPrice = sellPrice;
		this.phone = phone;
		this.description = description;
		this.expireTime = expireTime;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getId(){ return id;}

	public String getUserId() {
		return userId;
	}

	public int getArea() {
		return area;
	}
	
	public BuildingType getBuildingType() {
		return buildingType;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getImageURL() {
		return imageURL;
	}
	
	public DealType getDealType() {
		return dealType;
	}
	
	public int getBasePrice() {
		return basePrice;
	}
	
	public int getRentPrice() {
		return rentPrice;
	}
	
	public int getSellPrice() {
		return sellPrice;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getDescription() {
		return description;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
	
	public int getPrice() {
		if(dealType == DealType.sell)
			return getSellPrice();
		else
			return getRentPrice();
	}

	public void replaceSpecialChars(){
		id = Message.replaceSpecialChars(id);
		userId = Message.replaceSpecialChars(userId);
		address = Message.replaceSpecialChars(address);
		imageURL = Message.replaceSpecialChars(imageURL);
		phone = Message.replaceSpecialChars(phone);
		description = Message.replaceSpecialChars(description);
	}
}

