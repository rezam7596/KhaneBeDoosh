package domain.house;

import domain.Database;
import domain.user.RealEstate;
import domain.user.User;
import domain.user.UserDB;
import domain.user.realstate.RequestServer;
import domain.user.realstate.ServerResponseType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HouseDB {
    private static HouseDB houseDB = null;

    private HouseDB(){
        Connection connection = Database.getConnection();
        Statement stmt;
        try {
            stmt = connection.createStatement();
            String sql = "CREATE TABLE Houses " +
                    "(ID             INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " houseID        CHAR(100) UNIQUE , " +
                    " userID         INT NOT NULL, " +
                    " area           REAL   NOT NULL, " +
                    " buildingType   CHAR(20)   NOT NULL, " +
                    " address        CHAR(200), " +
                    " imageURL       CHAR(200), " +
                    " dealType       CHAR(20)   NOT NULL, " +
                    " basePrice      REAL, " +
                    " rentPrice      REAL, " +
                    " sellPrice      REAL, " +
                    " phone          CHAR(20), " +
                    " description    CHAR(200)," +
                    "CONSTRAINT FK_HouseOwner FOREIGN KEY (userID) REFERENCES Users(UserId))";
            stmt.executeUpdate(sql);
            stmt.close();
        }catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return;
        }
    }

    public static HouseDB getInstance(){
        if(houseDB == null)
            houseDB = new HouseDB();
        return houseDB;
    }

    public boolean addHouse (House newHouse) {
        try {
            Connection connection = Database.getConnection();
            String sql = "INSERT INTO Houses (houseID,userID,area,buildingType," +
                        "address,imageURL,dealType,basePrice," +
                        "rentPrice,sellPrice,phone,description) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, newHouse.getId());
            statement.setInt(2, Integer.parseInt(newHouse.getUserId()));
            statement.setInt(3, newHouse.getArea());
            statement.setString(4, newHouse.getBuildingType().toString());
            statement.setString(5, newHouse.getAddress());
            statement.setString(6, newHouse.getImageURL());
            statement.setString(7, newHouse.getDealType().getDealType());
            statement.setDouble(8, newHouse.getBasePrice());
            statement.setDouble(9, newHouse.getRentPrice());
            statement.setDouble(10, newHouse.getSellPrice());
            statement.setString(11, newHouse.getPhone());
            statement.setString(12, newHouse.getDescription());

            statement.executeUpdate();
            statement.close();
            return true;
        }catch (Exception e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return false;
        }
    }

    public boolean addHouses (List<House> newHouses) {
        try {
            Connection connection = Database.getConnection();
            connection.setAutoCommit(false);
//            Statement stmt = connection.createStatement();
            PreparedStatement statement = null;
            for (House newHouse : newHouses) {
                String sql = "INSERT INTO Houses (houseID,userID,area,buildingType," +
                        "address,imageURL,dealType,basePrice," +
                        "rentPrice,sellPrice,phone,description) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                statement = connection.prepareStatement(sql);
                statement.setString(1, newHouse.getId());
                statement.setInt(2, Integer.parseInt(newHouse.getUserId()));
                statement.setInt(3, newHouse.getArea());
                statement.setString(4, newHouse.getBuildingType().toString());
                statement.setString(5, newHouse.getAddress());
                statement.setString(6, newHouse.getImageURL());
                statement.setString(7, newHouse.getDealType().getDealType());
                statement.setDouble(8, newHouse.getBasePrice());
                statement.setDouble(9, newHouse.getRentPrice());
                statement.setDouble(10, newHouse.getSellPrice());
                statement.setString(11, newHouse.getPhone());
                statement.setString(12, newHouse.getDescription());
                statement.executeUpdate();
            }
            connection.commit();
            statement.close();
            connection.setAutoCommit(true);
            return true;
        }catch (Exception e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return false;
        }
    }

    public List<House> searchAllHouses(int minimumArea, BuildingType buildingType, DealType dealType, int maximumPrice){
        List<House> houses = new ArrayList<>();
        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement;
//            Statement stmt = connection.createStatement();
            String sql = "SELECT * FROM Houses WHERE" + " area >= ? ";
            if(buildingType != null)
                sql += " AND buildingType = ? ";
            if(dealType != null)
                sql += " AND dealType = ? ";
            if(maximumPrice != Integer.MAX_VALUE)
                sql += ((dealType == DealType.rent)?"rentPrice":"sellPrice") + " <= ? ";

            statement = connection.prepareStatement(sql);
            int parameterSet = 0;
            statement.setInt(++parameterSet, minimumArea);
            if(buildingType != null)
                statement.setString(++parameterSet, buildingType.toString());
            if(dealType != null)
                statement.setString(++parameterSet, dealType.getDealType());
            if(maximumPrice != Integer.MAX_VALUE)
                statement.setInt(++parameterSet, maximumPrice);
            ResultSet rs = statement.executeQuery();

            while ( rs.next() ) {
                houses.add(new House(
                        rs.getString("houseID"),
                        String.valueOf(rs.getInt("userID")),
                        rs.getInt("area"),
                        rs.getString("buildingType").equals(BuildingType.villa.name())?BuildingType.villa:BuildingType.apartment,
                        rs.getString("address"),
                        rs.getString("imageURL"),
                        rs.getString("dealType").equals(DealType.sell.name())?DealType.sell:DealType.rent,
                        rs.getInt("basePrice"),
                        rs.getInt("rentPrice"),
                        rs.getInt("sellPrice"),
                        rs.getString("phone"),
                        rs.getString("description"),
                        null
                        ));
            }
            rs.close();
            statement.close();
            return houses;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }
    }

    public House findHouseById (String houseId){
        try {
            Connection connection = Database.getConnection();
            PreparedStatement stmt;
            String sql = "SELECT * FROM Houses WHERE houseID = ?;";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, houseId);
            ResultSet rs = stmt.executeQuery();
            House foundHouse = null;
            if (rs.next()) {
                foundHouse = new House(
                        rs.getString("houseID"),
                        String.valueOf(rs.getInt("userID")),
                        rs.getInt("area"),
                        rs.getString("buildingType").equals(BuildingType.villa.name()) ? BuildingType.villa : BuildingType.apartment,
                        rs.getString("address"),
                        rs.getString("imageURL"),
                        rs.getString("dealType").equals(DealType.sell.name()) ? DealType.sell : DealType.rent,
                        rs.getInt("basePrice"),
                        rs.getInt("rentPrice"),
                        rs.getInt("sellPrice"),
                        rs.getString("phone"),
                        rs.getString("description"),
                        null
                );
            }
            rs.close();
            stmt.close();
            //find owner of house and if it was RealEstate get full info from its server
            User owner = UserDB.getInstance().getUserByUserId(foundHouse.getUserId());
            if(owner instanceof RealEstate)
                return RequestServer.getHouse(((RealEstate) owner).getURL(), houseId, ServerResponseType.JSON);
            else
                return foundHouse;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }
    }

    public boolean deleteAllHousesFromUser(User user){
        try {
            Connection connection = Database.getConnection();
            PreparedStatement stmt;
            String sql = "DELETE from Houses where userID= ?;";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(user.getUserId())); ///????
            stmt.executeUpdate();
            return true;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return false;
        }
    }

    public List<House> getAllHousesFromUser(User user){
        try {
            Connection connection = Database.getConnection();
            PreparedStatement stmt;
            String sql = "SELECT * from Houses where userID=?;";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(user.getUserId()));
            ResultSet rs = stmt.executeQuery();
            List<House> foundHouses = new ArrayList<>();
            while (rs.next()) {
                foundHouses.add(new House(
                        rs.getString("houseID"),
                        String.valueOf(rs.getInt("userID")),
                        rs.getInt("area"),
                        rs.getString("buildingType").equals(BuildingType.villa.name()) ? BuildingType.villa : BuildingType.apartment,
                        rs.getString("address"),
                        rs.getString("imageURL"),
                        rs.getString("dealType").equals(DealType.sell.name()) ? DealType.sell : DealType.rent,
                        rs.getInt("basePrice"),
                        rs.getInt("rentPrice"),
                        rs.getInt("sellPrice"),
                        rs.getString("phone"),
                        rs.getString("description"),
                        null
                ));
            }
            rs.close();
            stmt.close();
            return foundHouses;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }
    }

    public List<House> getAllHouses(){
        try {
            Connection connection = Database.getConnection();
            PreparedStatement stmt;
            String sql = "SELECT * from Houses;";
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            List<House> foundHouses = new ArrayList<>();
            while (rs.next()) {
                foundHouses.add(new House(
                        rs.getString("houseID"),
                        String.valueOf(rs.getInt("userID")),
                        rs.getInt("area"),
                        rs.getString("buildingType").equals(BuildingType.villa.name()) ? BuildingType.villa : BuildingType.apartment,
                        rs.getString("address"),
                        rs.getString("imageURL"),
                        rs.getString("dealType").equals(DealType.sell.name()) ? DealType.sell : DealType.rent,
                        rs.getInt("basePrice"),
                        rs.getInt("rentPrice"),
                        rs.getInt("sellPrice"),
                        rs.getString("phone"),
                        rs.getString("description"),
                        null
                ));
            }
            rs.close();
            stmt.close();
            return foundHouses;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }
    }
}
