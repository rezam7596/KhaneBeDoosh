package domain.house;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.Date;

public class HouseJsonDeserializer extends StdDeserializer<House> {
	
	protected HouseJsonDeserializer() {
		this(null);
	}
	
	protected HouseJsonDeserializer(Class<?> vc) {
		super(vc);
	}
	
	@Override
	public House deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
			throws IOException, JsonProcessingException {
		JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);
		String id = jsonNode.get("id").asText();
		int area = jsonNode.get("area").asInt();
		String address = jsonNode.has("address") ? jsonNode.get("address").asText() : null;
		DealType dealType = (jsonNode.get("dealType").asInt() == 0) ? DealType.sell : DealType.rent;
		int sellPrice = 0, basePrice = 0, rentPrice = 0;
		if(dealType == DealType.sell)
			sellPrice = jsonNode.get("price").get("sellPrice").asInt();
		else{
			basePrice = jsonNode.get("price").get("basePrice").asInt();
			rentPrice = jsonNode.get("price").get("rentPrice").asInt();
		}
		String phone = jsonNode.has("phone") ? jsonNode.get("phone").asText() : null;
		String description = jsonNode.has("description") ? jsonNode.get("description").asText() : null;
		BuildingType buildingType = jsonNode.get("buildingType").asText().equals("آپارتمان") ? BuildingType.apartment : BuildingType.villa;
		//TODO: handle expireTime ("expireTime": "2018-02-11")
		Date expireTime = null;
		String imageURL = jsonNode.get("imageURL").asText();
		
		return new House(id, "2", area, buildingType, address, imageURL, dealType, basePrice, rentPrice, sellPrice, phone, description, expireTime);
	}
}
