package domain.house;

import domain.user.RealEstate;
import domain.user.User;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class UpdateHousesWhenExpired extends TimerTask {
    RealEstate owner;
    public UpdateHousesWhenExpired(RealEstate owner){
        this.owner = owner;
    }
    public void run()
    {
        HouseDB.getInstance().deleteAllHousesFromUser(owner);
        HouseDB.getInstance().addHouses(owner.getHouses());
        new Timer().schedule(new UpdateHousesWhenExpired(owner), new Date(owner.getExpireTime()));
    }
}