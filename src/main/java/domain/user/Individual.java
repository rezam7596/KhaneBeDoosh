package domain.user;

import domain.Database;
import domain.Message;
import domain.bank.Payment;
import domain.house.House;

import domain.house.HouseDB;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.message.*;
import org.apache.http.client.entity.*;
import org.apache.http.util.*;
import org.apache.http.*;
import org.apache.http.impl.client.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class Individual extends User{

    protected String phone;
    protected double balance;
    protected String password;
    protected List<String> paidHouseNums;
    protected boolean admin;

    Individual(String name, String password, int userId){
        super(name, userId);
        this.password = password;
        this.phone = "00000000000";
        paidHouseNums = new ArrayList<>();
        admin = false;
    }
    Individual(String name, int userId, String password, String phone, double balance, List<String> paidHouses, boolean admin){
        super(name, userId);
        this.password = password;
        this.phone = phone;
        this.balance = balance;
        this.paidHouseNums = paidHouses;
        this.admin = admin;
    }

    public String getPhone() {
        return phone;
    }

    public double getBalance() {
        UserDB.getInstance().readUserFromDB(this);
        return balance;
    }

    public void setBalance() {
        UserDB.getInstance().readUserFromDB(this);
        balance -= 1000;
        UserDB.getInstance().saveUserToDB(this);
    }

    public String getPassword() {
        UserDB.getInstance().readUserFromDB(this);
        return password;
    }

    public void paidHouse(String houseId){
        UserDB.getInstance().readUserFromDB(this);
        //TODO: why bottom line is necessary or there is an exception
        paidHouseNums = new ArrayList<>(paidHouseNums);
        paidHouseNums.add(houseId);
        UserDB.getInstance().saveUserToDB(this);
    }
    public boolean isHouseNumPaid(String houseId){
        UserDB.getInstance().readUserFromDB(this);
        for (int i = 0; i< paidHouseNums.size(); i++ ){
            if(paidHouseNums.get(i).equals(houseId))
                return true;
        }
        return false;
    }
	
    @Override
	public List<House> getHouses() {
        return HouseDB.getInstance().getAllHousesFromUser(this);
	}

    public boolean isAdmin() {
        UserDB.getInstance().readUserFromDB(this);
        return admin;
    }

    public void setAdmin(boolean admin) {
        UserDB.getInstance().readUserFromDB(this);
        this.admin = admin;
        UserDB.getInstance().saveUserToDB(this);
    }
	
	public void addHouse(House house){
		house.setUserId(this.getUserId());
		HouseDB.getInstance().addHouse(house);
	}
	
	public Message pay(int credit){
        UserDB.getInstance().readUserFromDB(this);
		Message responseMessage = Payment.pay(this, credit);
		if (responseMessage.getSuccess()){
		    balance += credit;
        }
        UserDB.getInstance().saveUserToDB(this);
		return responseMessage;
	}
	
	@Override
	public String getUserInfo(){
        UserDB.getInstance().readUserFromDB(this);
    	String userInfo = "";
    	userInfo += "{";
    	userInfo += "\"userid\":\"" + this.getUserId() + "\",";
    	userInfo += "\"username\":\"" + this.name + "\",";
    	userInfo += "\"phone\":\"" + this.phone + "\",";
    	userInfo += "\"balance\":" + this.balance + "";
    	userInfo += "}";
    	return userInfo;
	}
}
