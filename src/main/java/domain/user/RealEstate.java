package domain.user;

import domain.Database;
import domain.house.House;
import domain.house.HouseDB;
import domain.house.UpdateHousesWhenExpired;
import domain.user.realstate.RequestServer;
import domain.user.realstate.ServerResponse;
import domain.user.realstate.ServerResponseType;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;

public class RealEstate extends User{
	protected String URL;
	private static List<Integer> scheduledUser = new ArrayList<>();

	RealEstate(String name, String URL, int userId) {
        super(name, userId);
     	this.URL = URL;

     	if(! scheduledUser.contains(Integer.parseInt(this.getUserId())) ) {
			HouseDB.getInstance().addHouses(this.getHouses());
			new Timer().schedule(new UpdateHousesWhenExpired(this), new Date(this.getExpireTime()));
			scheduledUser.add(Integer.parseInt(this.getUserId()));
		}
    }

	public void setURL(String URL) {
    	this.URL = URL;
    	UserDB.getInstance().saveUserToDB(this);
	}

	public String getURL() {
    	UserDB.getInstance().readUserFromDB(this);
		return URL;
	}

	@Override
	public List<House> getHouses() {
		return RequestServer.getHouses(URL, ServerResponseType.JSON).getData();
	}

	public long getExpireTime() {
		return RequestServer.getHouses(URL, ServerResponseType.JSON).getExpireTime();
	}

	@Override
	public String getUserInfo(){
		UserDB.getInstance().readUserFromDB(this);
		String userInfo = "";
		userInfo += "{";
		userInfo += "\"userid\":\"" + this.getUserId() + "\",";
		userInfo += "\"username\":\"" + this.name + "\",";
		userInfo += "\"phone\":\"" + this.URL + "\"";
		userInfo += "}";
		return userInfo;
	}
}
