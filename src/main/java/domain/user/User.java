package domain.user;

import domain.Database;
import domain.house.House;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public abstract class User {
	protected String name;
	private int userId;
	
	User(String name, int userId) {
		this.name = name;
        this.userId = userId;
	}

    public String getUserId() {
        return Integer.toString(userId);
    }

    public String getName() {
		return this.name;
	}
	abstract public List<House> getHouses();
	abstract public String getUserInfo();
}

