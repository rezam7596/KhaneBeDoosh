package domain.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Database;
import domain.house.House;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.sql.*;

public class UserDB {
    static UserDB userDB = null;
	
    private UserDB(){
        Statement stmt;
        Connection connection = Database.getConnection();
        try {
            stmt = connection.createStatement();
            String sql = "CREATE TABLE Users " +
                    "(userId        INT   PRIMARY KEY NOT NULL ," +
                    " name          CHAR(50) NOT NULL," +
                    " password      CHAR(50), " +
                    " phone         CHAR(20), " +
                    " balance       REAL, " +
                    " paidHouses    VARCHAR(10000), " +
                    " admin         INTEGER, " +
                    " URL           VARCHAR(2083))";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    public static UserDB getInstance(){
        if(userDB == null) {
            userDB = new UserDB();
            userDB.addUser(new Individual("Homayoon", "123", 1));
            userDB.addUser(new RealEstate("KhaneBeDoosh", "http://139.59.151.5:6664/khaneBeDoosh/v2", 2));
        }
        return userDB;
    }

    public User getUser(String name) {
        Connection connection = Database.getConnection();
        PreparedStatement statement;
        try {
            String sql = "SELECT * FROM Users WHERE " + " name = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1,name);
            ResultSet rs = statement.executeQuery();
            User user = buildUserFromResultSearch(rs);
            rs.close();
            statement.close();
            return user;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }
    }
    public User getUser(String name, String password) {
        Connection connection = Database.getConnection();
        PreparedStatement statement;
        try {
            String sql = "SELECT * FROM Users WHERE name = ? AND password = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1,name);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();
            User user = buildUserFromResultSearch(rs);
            rs.close();
            statement.close();
            return user;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return null;
    }

    
    public User getUserByUserId(String userId){
        Connection connection = Database.getConnection();
        PreparedStatement statement;
        try {
            String sql = "SELECT * FROM Users WHERE userId= ?;";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, Integer.parseInt(userId));
            ResultSet rs = statement.executeQuery();
            User user = buildUserFromResultSearch(rs);
            rs.close();
            statement.close();
            return user;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }
    }

    private User buildUserFromResultSearch(ResultSet rs) throws SQLException, java.io.IOException {
        User user = null;
        String[] paidHousesArray;
        if (rs.next()) {
            String tmp = rs.getString("userId");
            if (rs.getString("URL") == null) {
                paidHousesArray = new ObjectMapper().readValue(rs.getString("paidHouses"), String[].class);
                user = new Individual(
                        rs.getString("name"),
                        rs.getInt("userId"),
                        rs.getString("password"),
                        rs.getString("phone"),
                        rs.getDouble("balance"),
                        Arrays.asList(paidHousesArray),
                        rs.getBoolean("admin")
                );
            }
            else
                user = new RealEstate(
                        rs.getString("name"),
                        rs.getString("URL"),
                        rs.getInt("userId")
                );
        }
        return user;
    }

    public List<User> getAllUsers() {
        Connection connection = Database.getConnection();
        Statement statement;
        List<User> users = new ArrayList<>();
        try {
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery( "SELECT * FROM Users;");
            while (! rs.isAfterLast())
                users.add(buildUserFromResultSearch(rs));
            rs.close();
            statement.close();
            return users;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }
	}

	public boolean addUser(User newUser){
        Connection connection = Database.getConnection();
        PreparedStatement statement;
        try {
            String sql;
            if (newUser instanceof Individual) {
                String paidHouses = new ObjectMapper().writeValueAsString(((Individual) newUser).paidHouseNums);
                sql = "INSERT INTO Users (userId,name,password,balance,phone,paidHouses,admin) VALUES (?,?,?,?,?,?,?);";
                statement = connection.prepareStatement(sql);
                statement.setInt(1, Integer.parseInt(newUser.getUserId()));
                statement.setString(2, newUser.name);
                statement.setString(3, ((Individual) newUser).password);
                statement.setDouble(4, ((Individual) newUser).balance);
                statement.setString(5, ((Individual) newUser).phone);
                statement.setString(6, paidHouses);
                statement.setBoolean(7, ((Individual) newUser).isAdmin());
            }
            else{
                sql = "INSERT INTO Users (userId,name,URL) VALUES (?,?,?);";
                statement = connection.prepareStatement(sql);
                statement.setInt(1, Integer.parseInt(newUser.getUserId()));
                statement.setString(2, newUser.name);
                statement.setString(3, ((RealEstate) newUser).URL);

            }

            statement.executeUpdate();
            statement.close();
            return true;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return false;
        }
    }

    public boolean readUserFromDB(User user){
        User newUser = getUserByUserId(user.getUserId());
        if (newUser == null)
            return false;

        user.name = newUser.name;
        if(user instanceof Individual){
            ((Individual) user).phone = ((Individual) newUser).phone;
            ((Individual) user).balance = ((Individual) newUser).balance;
            ((Individual) user).password = ((Individual) newUser).password;
            ((Individual) user).paidHouseNums = ((Individual) newUser).paidHouseNums;
            ((Individual) user).admin = ((Individual) newUser).admin;
        }
        else
            ((RealEstate) user).URL = ((RealEstate) newUser).URL;

        return true;
    }

    public boolean saveUserToDB(User user){
        Connection connection = Database.getConnection();
        PreparedStatement statement;
        try {
            String sql;
            if (user instanceof Individual) {
                String paidHouses = new ObjectMapper().writeValueAsString(((Individual) user).paidHouseNums);
                sql = "UPDATE Users SET name= ?, password= ?, balance= ?, phone= ?, paidHouses=?, admin=? WHERE userId= ?;";
                statement = connection.prepareStatement(sql);
                statement.setString(1, user.name);
                statement.setString(2, ((Individual) user).password);
                statement.setDouble(3, ((Individual) user).balance);
                statement.setString(4, ((Individual) user).phone);
                statement.setString(5, paidHouses);
                statement.setBoolean(6, ((Individual) user).isAdmin());
                statement.setInt(7, Integer.parseInt(user.getUserId()));
            }
            else {
                sql = "UPDATE Users SET name= ? ,URL= ? WHERE userId= ?;";
                statement = connection.prepareStatement(sql);
                statement.setString(1, user.name);
                statement.setString(2, ((RealEstate) user).URL);
                statement.setInt(3, Integer.parseInt(user.getUserId()));

            }
            statement.executeUpdate();
            statement.close();
            return true;
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return false;
        }
    }
}
