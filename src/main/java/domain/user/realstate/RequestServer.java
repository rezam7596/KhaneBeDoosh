package domain.user.realstate;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Message;
import domain.house.House;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RequestServer {
	
	static Message simpleGetRequestToServer(String url){
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet httpGetRequest = new HttpGet(url);
		try {
			CloseableHttpResponse response = httpClient.execute(httpGetRequest);
			BufferedReader contentReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			Message message = new Message(response.getStatusLine().getStatusCode());
			message.setSuccess((response.getStatusLine().getStatusCode() / 100) == 2);
			message.setMessage(contentReader.readLine());
			return message;
		}catch (Exception e){
			return new Message(false, HttpStatus.SC_NOT_ACCEPTABLE, e.getMessage());
		}
	}
	
	public static House getHouse(String url, String id, ServerResponseType serverResponseType){
		Message response = simpleGetRequestToServer(url + "/house/" + id);
		if(serverResponseType == ServerResponseType.JSON) {
			try {
				ObjectMapper objectMapper = new ObjectMapper().enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
				ServerResponse serverResponse = objectMapper.readValue(response.getMessage(), ServerResponse.class);
				System.out.println("read a house form realEstate server");
				return serverResponse.getData().get(0);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				return null;
			}
		}
		System.out.println("Invalid Type for RealEstate Server Response");
		return null;
	}
	
	
	public static ServerResponse getHouses(String url, ServerResponseType serverResponseType){
		Message response = simpleGetRequestToServer(url + "/house");
		if(serverResponseType == ServerResponseType.JSON) {
			try {
				ServerResponse serverResponse = new ObjectMapper().readValue(response.getMessage(), ServerResponse.class);
				System.out.println("read " + serverResponse.getData().size() + " houses form realEstate server");
				return serverResponse;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				return null;
			}
		}
		System.out.println("Invalid Type for RealEstate Server Response");
		return null;
	}
}
