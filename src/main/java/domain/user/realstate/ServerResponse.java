package domain.user.realstate;

import domain.house.House;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class ServerResponse {
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
	
	public List<House> getData() {
		return data;
	}

	
	public void setData(List<House> data) {
		this.data = data;
	}

	public long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}

	String result;
	long expireTime;
	List<House> data;
}
