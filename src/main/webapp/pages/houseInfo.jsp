<%--
  Created by IntelliJ IDEA.
  User: farima
  Date: 2/23/18
  Time: 1:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.*, domain.*, domain.house.House" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>house information</title>
</head>
<body>
<p>
    <span>Username: ${user.name}</span>
    &#9 &#9
    <span>Credit: ${user.balance} toman</span>
</p>
<div>
    <table border="1">
        <!--TODO: if statements are not working!!! -->
        <tr>
            <td>building type: <c:out value="${targetHouse.buildingType}"/></td>
            <td>area: <c:out value="${targetHouse.area}"/></td>
            <td>deal type: <c:out value="${targetHouse.dealType}"/></td>
            <c:if test="${targetHouse.dealType.dealType == 'sell'}">
                <td>sell price: <c:out value="${targetHouse.sellPrice}"/></td>
            </c:if>
            <c:if test="${targetHouse.dealType.dealType == 'rent'}">
                <td>base price: <c:out value="${targetHouse.basePrice}"/></td>
                <td>rent price: <c:out value="${targetHouse.rentPrice}"/></td>
            </c:if>
            <td>address: <c:out value="${targetHouse.address}"/></td>
            <td><a href="<c:out value="${targetHouse.imageURL}"/>">image</a></td>
            <td>description:
                <c:if test="${not empty targetHouse.description}">
                    <c:out value="${targetHouse.description}"/>
                </c:if>
                <c:if test="${empty targetHouse.description}">
                    <c:out value="ندارد"/>
                </c:if>
            </td>
            <td>
                <form action="getPhone" method="GET">
                    <input type="text" value="${targetHouse.id}" hidden name="houseId"/>
                    <c:choose>
                        <c:when test = "${owner_phone == 'get_request'}">
                            <input type="submit" value="دریافت شماره مالک/مشاور"/>
                        </c:when>

                        <c:when test = "${owner_phone == 'show'}">
                            <td> شماره مالک/مشاور: <c:out value="${targetHouse.phone}"/></td>
                        </c:when>

                        <c:otherwise>
                            <td> اعتبار شما برای دریافت شماره مالک/مشاور کافی نیست. </td>
                        </c:otherwise>
                    </c:choose>
                </form>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
