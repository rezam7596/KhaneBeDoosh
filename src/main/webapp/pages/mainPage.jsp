<%--
  Created by IntelliJ IDEA.
  User: farima
  Date: 2/18/18
  Time: 12:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*, domain.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
    <head>
        <title>main page</title>
    </head>
    <body>
        <p>
            <span>
                Username: ${user.name}
            </span>
            &#9 &#9
            <span>
                Credit: ${user.balance} toman
            </span>
        </p>
        <div>
            <c:if test="${not empty messageToShowInTop}">
                <h3><c:out value="${messageToShowInTop}"/></h3>
            </c:if>
        </div>
        <form action="search" method="GET">
            <input type="text" name="minimum_area" placeholder="حداقل متراژ"/><br>
            <select name="building_type">
                <option value="not_selected" selected> نوع ملک </option>
                <option value="villa"> ویلایی </option>
                <option value="apartment"> آپارتمانی </option>
            </select><br>
            <select name="deal_type">
                <option value="not_selected" selected> نوع قرارداد </option>
                <option value="sell"> خرید </option>
                <option value="rent"> اجاره </option>
            </select><br>
            <input type="text" name="maximum_price" placeholder="حداکثر قیمت"/><br>
            <input type="submit" value="جست‌و‌جو"/>
        </form>
        <form action="addNewHouse" method="GET">
            <select name="building_type">
                <option value="not_selected" selected> نوع ساختمان </option>
                <option value="villa"> ویلایی </option>
                <option value="apartment"> آپارتمان </option>
            </select><br>
            <input type="text" name="area" placeholder="متراژ"/><br>
            <select name="deal_type">
                <option value="not_selected" selected> نوع قرارداد </option>
                <option value="sell"> خرید </option>
                <option value="rent"> اجاره </option>
            </select><br>
            <input type="text" name="price" placeholder="قیمت فروش/اجاره"/><br>
            <input type="text" name="address" placeholder="آدرس"/><br>
            <input type="text" name="phone" placeholder="شماره تماس"/><br>
            <input type="text" name="description" placeholder="توضیحات"/><br>
            <input type="submit" value="اضافه کردن خانه جدید"/>
        </form>
        <form action="increaseCredit" method="GET">
            <input type="text" name="balance" placeholder="اعتبار"/><br>
            <input type="submit" value="افزایش اعتبار"/>
        </form>
    </body>
</html>
