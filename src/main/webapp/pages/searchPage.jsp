<%@ page import="java.util.*, domain.house.House" %>
<%@ page import="domain.house.House" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head><title>Search Result</title></head>
<body>
<p>
    <span>Username: ${user.name}</span>
    &#9 &#9
    <span>Credit: ${user.balance} toman</span>
</p>
<div>
    <table border="1">
        <!--TODO: if statements is not working!!! -->
        <c:forEach var="house" items="${houses}">
            <tr>
                <c:if test="${house.dealType.dealType == 'sell'}">
                    <td>sell price: <c:out value="${house.sellPrice}"/></td>
                </c:if>
                <c:if test="${house.dealType.dealType == 'rent'}">
                    <td>base price: <c:out value="${house.basePrice}"/></td>
                    <td>rent price: <c:out value="${house.rentPrice}"/></td>
                </c:if>
                <td>area: <c:out value="${house.area}"/></td>
                <td>deal type: <c:out value="${house.dealType}"/></td>
                <td><a href="<c:out value="${house.imageURL}"/>">image</a></td>
                <td>
                    <form action="houseInfo" method="GET">
                        <input type="text" value="${house.id}" hidden name="houseId"/>
                        <input type="submit" value="اطلاعات بیشتر"/>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>
